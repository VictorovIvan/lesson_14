package com.homework.lesson14.main_screen.domain

import com.homework.lesson14.main_screen.data.api.NewsRepo
import com.homework.lesson14.main_screen.domain.model.ArticleDomainModel

class NewsInteractor(private val repository: NewsRepo) {
    suspend fun getNews(): List<ArticleDomainModel> {
        return repository.getNews()
    }
}