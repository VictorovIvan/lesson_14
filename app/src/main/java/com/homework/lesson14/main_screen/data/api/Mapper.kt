package com.homework.lesson14.main_screen.data.api

import com.homework.lesson14.main_screen.data.api.model.ArticleModel
import com.homework.lesson14.main_screen.domain.model.ArticleDomainModel


fun ArticleModel.toDomain() = ArticleDomainModel(
    author = this.author,
    title = this.title,
    description = this.description,
    url = this.url,
    publishedAt = this.publishedAt
)


