package com.homework.lesson14.main_screen.data.api

import com.homework.lesson14.main_screen.domain.model.ArticleDomainModel

interface NewsRepo {
   suspend fun getNews(): List<ArticleDomainModel>
}