package com.homework.lesson14.main_screen.data.api

import com.homework.lesson14.main_screen.domain.model.ArticleDomainModel

class NewsRepoImpl (private val source: NewsRemoteSource): NewsRepo {
    override suspend fun getNews(): List<ArticleDomainModel> {
        return source.getNews().articles.map {
            it.toDomain()
        }

    }
}