package com.homework.lesson14.main_screen.data.api

import com.homework.lesson14.main_screen.data.api.model.TopNewsModel

class NewsRemoteSource(private val api: NewsApi) {
    suspend fun getNews(): TopNewsModel = api.getTopHeadLines()
}